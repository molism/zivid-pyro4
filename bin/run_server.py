
# coding=utf-8

"""
"""

__author__ = "Eirik Njåstad"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Eirik Njåstad"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import argparse
import threading

import Pyro4
Pyro4.config.SERIALIZERS_ACCEPTED |= set(['pickle'])
Pyro4.config.SERIALIZER = 'pickle'

import zivid_pyro4.server


ap = argparse.ArgumentParser()
ap.add_argument('--host', type=str, default='0.0.0.0')
ap.add_argument('--port', type=int, default=9990)
ap.add_argument('--object_id', type=str, default='zivid_server',
                help="""An object ID to know the Zivid server object by. It must be unique for the daemon.""")
args = ap.parse_args()

zivid_server = Pyro4.expose(zivid_pyro4.server.ZividServer)()
daemon = Pyro4.Daemon(host=args.host, port=args.port)
zivid_server_uri = daemon.register(zivid_server, objectId=args.object_id)
daemon_thread = threading.Thread(target=daemon.requestLoop, daemon=True)
daemon_thread.start()
