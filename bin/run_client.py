# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Eirik Njåstad"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import argparse

import numpy as np
import Pyro4
Pyro4.config.SERIALIZERS_ACCEPTED |= set(['pickle'])
Pyro4.config.SERIALIZER = 'pickle'


ap = argparse.ArgumentParser()
ap.add_argument('--host', type=str, default='127.0.0.1')
ap.add_argument('--port', type=int, default=9990)
ap.add_argument('--object_id', type=str, default='zivid_server')
args = ap.parse_args()

zivid_server = Pyro4.Proxy('PYRO:{object_id}@{host}:{port}'
                           .format(**args.__dict__))
