# coding=utf-8

"""
"""

__author__ = "Eirik Njåstad"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Eirik Njåstad"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading

import zivid
import numpy as np


class ZividServer:

    def __init__(self):
        self._data_lock = threading.Lock()
        # self._settings
        # self._cam = ...

    def capture(self):
        with self._data_lock:
            # self._frame = self._cam.capture(self._settings)
            # Test:
            pass

    @property
    def rgba(self):
        with self._data_lock:
            # return self._frame.point_cloud().copy_data("rgba")
            # Test data
            np.empty((5, 7, 3), dtype=np.float32)

    @property
    def point_cloud(self):
        with self._data_lock:
            # return self._frame.point_cloud().copy_data("xyz")
            # Test data
            self._rgba = np.empty((5, 7, 4), dtype=np.uint8)
